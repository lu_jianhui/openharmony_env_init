# 一键自动化配置OpenHarmony环境

## 目录

- [概述](#概述)
- [配置前说明](#配置前说明)
- [一键自动化配置](#一键自动化配置)
- [常见文件定位和说明](#常见文件定位和说明)
- [问题和建议](#问题和建议)

## 一、概述<a name="概述"></a>

本工程旨在对Ubuntu一键初始化配置环境，解决OpenHarmony的编译依赖问题，基于本脚本配置后配合[一键下载OpenHarmony代码](https://gitee.com/itopen/ohos_download.git)便能轻松掌控OpenHarmony的下载、编译。

当前建议使用稳定分支`Itopen-2.0-Release`，该分支是经过多次测试OK的，master分支可能随时会重构导致使用存在问题。

## 二、配置前说明<a name="配置前说明"></a>

当前支持以下版本，新的版本还在适配计划中：

Ubuntu18.04

Ubuntu20.04

Ubuntu22.04

![icon-note.gif](./img/icon/icon-note.gif)强烈推荐`Ubuntu20.04`，这既是OpenHarmony推荐的版本，也是小编当前使用的版本。

## 三、一键自动化配置<a name="一键自动化配置"></a>

## 3.1 自动化配置脚本介绍

当前脚本实现的配置功能如下，系统配置管理员只要执行**root环境配置**即可，个人账号只要执行**用户环境配置**即可，强烈不建议开发者直接使用root账号直接开发：

- root环境配置：必须是root账号或者具备root权限的账号

  - 配置/etc/apt/sources.list为国内源

  - 修改/usr/bin/sh链接/bin/bash

  - 安装基础软件
  - 安装git lfs

  - 安装repo

  - 创建/usr/include/asm链接/usr/include/x86_64-linux-gnu/asm

- 用户环境配置：没有用户限制

  - 配置.bashrc中的PS1
  - 配置tools小工具
  - 配置ssh
  - 配置git
  - 配置vim
  - 配置python3的下载源
  - 配置hb

- 独立的功能环境配置：必须是root账号或者具备root权限的账号

  - 配置/etc/apt/sources.list为国内源
  - 修改/usr/bin/sh链接/bin/bash
  - 创建/usr/include/asm链接/usr/include/x86_64-linux-gnu/asm 
  - 安装基础软件
  - 配置vim


![icon-note.gif](./img/icon/icon-note.gif)其中的vim是个人的配置，不需要可以安装完成后删除`~/.vim`和`~/.vimrc`即可，关于vim的操作可参考：[我的个人vim](https://gitee.com/itopen/vim)

## 3.2 自动化脚本使用

```shell
apt-get -f -y install ssh # ssh连接服务器必备, 直接在本机上操作可不用提前安装
apt-get -f -y install net-tools # ssh连接服务器必备, 直接在本机上操作可不用提前安装
apt-get -f -y install git
apt-get -f -y install dos2unix
git clone https://gitee.com/itopen/openharmony_env_init.git
cd openharmony_env_init
./build.sh
*******************************************
*   Welcome to init Ubuntu environment    *
* Please Choice Init Mode:                *
*   Install root environment   prese 1.1  *
*   Install user environment   prese 1.2  *
*   Update Source List         prese 1.3  *
*   Update sh to bash          prese 1.4  *
*   Create /usr/include/asm    prese 1.5  *
*   Apt Install Base Software  prese 1.6  *
*   Init myvim config          prese 1.7  *
*******************************************
1.1 # 输入你想操作的选项，然后根据提示一步步操作即可
```

## 四、常见文件定位和说明<a name="常见文件定位和说明"></a>

### 4.1 Ubuntu18.04一键配置环境问题说明

#### 4.1.1 pip3版本问题

```shell
==================================================
pip3 版本9.0.1太低, 不支持通过config配置python国内源方法
==================================================
```

该步骤是设置python3的下载源为国内源，但是由于Ubuntu18.04的pip3版本太低了，不支持`pip3 config`命令，所以设置失败，不过这不影响使用，如果想成功设置可以将Ubuntu18.04的python3升级到python3.8以上，同时将pip3也同步升级后即可。

#### 4.1.2 python3版本问题

```shell
===================================================================
Ubuntu18.04默认python3版本python3.6是太低了, 请将版本升级到python3.8或以上
===================================================================
```

该步骤是配置hb命令时报错，OpenHarmony的hb安装要求python3的最低版本要求是python3.8，而Ubuntu18.04的python3默认版本是python3.6，如果需要配置hb需要将python3升级到python3.8或者python3.10，不要更高，否则Ubuntu18.04可能不支持。hb命令会影响到小型系统（L1）和轻量型系统（L0）的编译，不影响标准系统（L2）的编译。

### 4.2 在Ubuntu22.04初始化root环境时报错git-lfs的问题

运行1.1的root初始化环境时出现如下问题

```shell
Err:9 https://packagecloud.io/github/git-lfs/ubuntu jammy InRelease
  The following signatures couldn't be verified because the public key is not available: NO_PUBKEY 6B05F25D762E3157
Reading package lists... Done
W: GPG error: https://packagecloud.io/github/git-lfs/ubuntu jammy InRelease: The following signatures couldn't be veri                                                 fied because the public key is not available: NO_PUBKEY 6B05F25D762E3157
E: The repository 'https://packagecloud.io/github/git-lfs/ubuntu jammy InRelease' is not signed.
N: Updating from such a repository can't be done securely, and is therefore disabled by default.
N: See apt-secure(8) manpage for repository creation and user configuration details.
```

该问题是因为替换`/etc/apt/sources.list`之前安装了`git-lfs`，在替换`sources.list`后不能识别之前的`public key`，解决办法是执行`sudo apt-get autoremove git-lfs`命令后再初始化环境，如果遇到类似的情况，同样处理。

## 五、问题和建议<a name="问题和建议"></a>

因`Gitee`无法评论，大家有什么问题或者建议可以到`OpenHarmony`开发者论坛的下面文章中评论

[如何优雅的一键适配OpenHarmony环境](https://forums.openharmony.cn/forum.php?mod=viewthread&tid=2804)



