#!/usr/bin/env bash

char_flag="*"
char_num=43

front_black='30'
front_red='31'
front_green='32'
front_yellow='33'
front_blue='34'
front_purple='35'
front_cyan='36'
front_white='37'

back_black='40'
back_red='41'
back_green='42'
back_yellow='43'
back_blue='44'
back_purple='45'
back_cyan='46'
back_white='47'

font_underline='04'
color_flash='05'

color_prefix='\e['
color_suffix='\e[0m'
color_sep=';'
color_flag='m'

function root_menu() {
  local dot_num=68
  cyan_flash=${color_prefix}${front_cyan}${color_sep}${color_flash}${color_flag}
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  printf "%-${dot_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
  echo -e "*                            ${cyan_flash}Note Tips${color_suffix}                             *"
  echo -e "*  ${yellow_black}root environment将会安装以下的功能:${color_suffix}                             *"
  echo -e "*    1、修改/etc/apt/sources.list内容                              *"
  echo -e "*    2、修改/usr/bin/sh链接/bin/bash                               *"
  echo -e "*    3、安装基础软件                                               *"
  echo -e "*    4、安装git lfs                                                *"
  echo -e "*    5、安装repo                                                   *"
  echo -e "*    6、创建/usr/include/asm链接/usr/include/x86_64-linux-gnu/asm  *"
  printf "%-${dot_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}

function user_menu() {
  cyan_flash=${color_prefix}${front_cyan}${color_sep}${color_flash}${color_flag}
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
  echo -e "*                ${cyan_flash}Note Tips${color_suffix}                *"
  echo -e "* ${yellow_black}user environment将会安装以下的功能:${color_suffix}     *"
  echo -e "*   1、配置.bashrc中的PS1                 *"
  echo -e "*   2、配置tools小工具                    *"
  echo -e "*   3、配置ssh                            *"
  echo -e "*   4、配置git                            *"
  echo -e "*   5、配置vim                            *"
  echo -e "*   6、配置python3的下载源                *"
  echo -e "*   7、配置hb                             *"
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}

function apt_list_menu() {
  local dot_num=48
  cyan_flash=${color_prefix}${front_cyan}${color_sep}${color_flash}${color_flag}
  yellow_black=${color_prefix}${front_yellow}${color_sep}${back_black}${color_flag}
  printf "%-${dot_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
  echo -e "*                  ${cyan_flash}Note Tips${color_suffix}                   *"
  echo -e "*  ${yellow_black}请选择要使用的source list:${color_suffix}                  *"
  echo -e "*    1、阿里源: aliyun                 press a *"
  echo -e "*    2、华为源: huaweicloud            press h *"
  echo -e "*    3、清华源: tsinghua               press t *"
  echo -e "*    4、官方源: ubuntu                 press u *"
  echo -e "*    5、默认不替换源                   press n *"
  printf "%-${dot_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}

function main_menu() {
  # 打印头
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"

  red_flash=${color_prefix}${front_red}${color_sep}${color_flash}${color_flag}
  cyan_black=${color_prefix}${front_cyan}${color_sep}${back_black}${color_flag}
  # 打印欢迎语句
  echo -e "*   ${red_flash}Welcome to init Ubuntu environment${color_suffix}    *"
  echo -e "* ${cyan_black}Please Choice Init Mode:${color_suffix}                *"
  echo -e "*   Install root environment   prese 1.1  *"
  echo -e "*   Install user environment   prese 1.2  *"
  echo -e "*   Update Source List         prese 1.3  *"
  echo -e "*   Update sh to bash          prese 1.4  *"
  echo -e "*   Create /usr/include/asm    prese 1.5  *"
  echo -e "*   Apt Install Base Software  prese 1.6  *"
  echo -e "*   Init myvim config          prese 1.7  *"

  # 打印尾
  printf "%-${char_num}s\n" "${char_flag}" | sed "s/ /${char_flag}/g"
}
