#!/usr/bin/env bash

set -e

ROOT_DIR=$(cd $(dirname $0); pwd)
param_file=${ROOT_DIR}/cmd_list
config_file=${ROOT_DIR}/.config
root_id_flag=0

export ETC_DIR=${ROOT_DIR}/etc
export TOOLS_DIR=${ROOT_DIR}/tools
export LSB_RELEASE=$(echo "$(lsb_release -c)" | awk '{print $2}')

source ./main_menu.sh

function set_passwd() {
  echo -e "请输入当前用户的密码"
  read passwd
  set +e
  $(echo ${passwd} | sudo -S apt-get install -y -f dos2unix > /dev/null 2>&1)
  if [[ $? -ne 0 ]]; then
    echo "用户: $(whoami)没有root权限或者密码错误, 请重试"
    exit 0
  fi
  set -e
  echo "passwd ${passwd}" >> ${config_file}
}

function check_user() {
  if [[ $(id -u) -eq 0 ]]; then
    export root_id_flag=1
    return 0
  fi

  if [ ! -f ${config_file} ]; then
    set_passwd
  else
    while read line; do
      if [[ $(echo ${line} | awk '{print $1}') == "passwd" ]]; then
        break
      fi
    done < ${config_file}

    if [[ $(echo ${line} | awk '{print $1}') == "" ]]; then
      set_passwd
    else
      passwd=$(echo ${line} | awk '{print $2}')
      set +e
      $(echo ${passwd} | sudo -S apt-get install -y -f dos2unix > /dev/null 2>&1)
      if [[ $? -ne 0 ]]; then
        sed -i '/passwd/d' ${config_file}
        set_passwd
      fi
      set -e
    fi
  fi

  while read line; do
    if [[ $(echo ${line} | awk '{print $1}') == "passwd" ]]; then
      break
    fi
  done < ${config_file}
  export passwd=$(echo ${line} | awk '{print $2}')
}

function change_sources_list() {
  echo "start check sources list..."
  local current_list=/etc/apt/sources.list
  local list_bak=/etc/apt/sources.list_bak
  local md5file=${ETC_DIR}/md5.txt

  echo "source_list is ${source_list}"
  if [ ! -f ${source_list} ]; then
    echo -e "${source_list} is not exist"
    return 1
  fi

  if [ ! -f ${current_list} ]; then
    return 0
  fi

  bash -c "md5sum ${source_list} ${current_list} > ${md5file}"
  ret=$(awk 'BEGIN {idx=0} {arr[idx]=$1; idx++} END {if (arr[0] == arr[1]) { print "OK" } else {print "FAILED"}}' ${md5file})
  bash -c "rm -rf ${md5file}"
  if [[ ${ret} == "OK" ]]; then
      echo "sources list is already update"
      return 2
  fi
  return 0
}

function change_source_list() {
  check_user
  local current_list=/etc/apt/sources.list
  local list_bak=/etc/apt/sources.list_bak
  if [[ ${root_id_flag} -eq 1 ]]; then
    bash -c "mv ${current_list} ${list_bak}"
    bash -c "cp ${source_list} ${current_list}"
    bash -c "apt-get update"
    bash -c "apt-get upgrade -y"
  else
    echo ${passwd} | sudo -S bash -c "mv ${current_list} ${list_bak}"
    echo ${passwd} | sudo -S bash -c "cp ${source_list} ${current_list}"
    echo ${passwd} | sudo -S bash -c "apt-get update"
    echo ${passwd} | sudo -S bash -c "apt-get upgrade -y"
  fi
}

function update_source_list() {
  apt_list_menu
  echo -e "请选择要替换的镜像源 [a/h/t/u/n]"
  read source_list
  if [[ ${source_list} == "a" || ${source_list} == "A" ]]; then
    echo "恭喜你选择的是阿里源"
    export source_list=${ETC_DIR}/${LSB_RELEASE}/aliyun/sources.list
  elif [[ ${source_list} == "h" || ${source_list} == "H" ]]; then
    echo "恭喜你选择的是华为源"
    export source_list=${ETC_DIR}/${LSB_RELEASE}/huawei/sources.list
  elif [[ ${source_list} == "t" || ${source_list} == "T" ]]; then
    echo "恭喜你选择的是清华源"
    export source_list=${ETC_DIR}/${LSB_RELEASE}/tsinghua/sources.list
  elif [[ ${source_list} == "u" || ${source_list} == "U" ]]; then
    echo "恭喜你选择的是官方源"
    export source_list=${ETC_DIR}/${LSB_RELEASE}/ubuntu/sources.list
  else
    echo "你选择的是不替换安装源"
  fi

  # 检查当前的源是否为要替换的源
  set +e
  change_sources_list
  if [[ $? -ne 0 ]]; then
    return 0
  fi
  set -e

  change_source_list
}

function update_sh_to_bash() {
  local cnt=$(ls -l /bin/sh | grep dash | wc -l)
  if [[ cnt == 0 ]]; then
    echo -e "/bin/sh is already link to /bin/bash"
  fi
  check_user
  if [[ ${root_id_flag} -eq 1 ]]; then
    bash -c "rm /bin/sh; ln -s /bin/bash /bin/sh"
  else
    echo ${passwd} | sudo -S bash -c "rm /bin/sh; ln -s /bin/bash /bin/sh" > /dev/null 2>&1
  fi
}

function create_asm_link() {
  check_user
  pushd /usr/include
  if [ ! -L "asm" ]; then
    echo "/usr/include/asm is not exist, start to create..."
    if [[ ${root_id_flag} -eq 1 ]]; then
      ln -s ./x86_64-linux-gnu/asm asm
    else
      echo ${passwd} | sudo -S bash -c "ln -s ./x86_64-linux-gnu/asm asm"
    fi
  fi
  popd
}

function git_lfs_install() {
  echo -e "start git lfs install..."
  check_user
  if [[ ${root_id_flag} -eq 1 ]]; then
    curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
    apt-get -y -f install git-lfs
    git lfs install
  else
    # echo ${passwd} | sudo -S bash -c "curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash"
    echo ${passwd} | sudo -S bash -c "apt-get -y -f install git-lfs"
    echo ${passwd} | sudo -S bash -c "git lfs install"
  fi
  echo -e "git lfs install end..."
}

function repo_install() {
  echo "start repo install..."
  check_user
  if [[ ${root_id_flag} -eq 1 ]]; then
    curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
    chmod a+x repo
    sed -i 's,#!/usr/bin/env python3,#!/usr/bin/env python,g' repo
    sed -i 's,#!/usr/bin/env python,#!/usr/bin/env python3,g' repo
    mv repo /usr/local/bin/
  else
    echo ${passwd} | sudo -S curl -s https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 > repo
    echo ${passwd} | sudo -S bash -c "chmod a+x repo"
    sed -i 's,#!/usr/bin/env python3,#!/usr/bin/env python,g' repo
    sed -i 's,#!/usr/bin/env python,#!/usr/bin/env python3,g' repo
    echo ${passwd} | sudo -S bash -c "mv repo /usr/local/bin/"
  fi
  echo "repo install end..."
}

function apt_install() {
  check_user
  local apt_list=${ETC_DIR}/${LSB_RELEASE}/apt_list.md
  if [ ! -f ${apt_list} ]; then
      echo -e "Please set list file dir"
      return 0
  fi

  export err_file=${ROOT_DIR}/err_file.txt
  rm -rf ${err_file:?}
  while read install_cmd; do
      local cnt=$(echo "${install_cmd}" | grep apt | wc -l)
      if [[ ${cnt} -gt 0 ]]; then
        echo -e "start ${install_cmd}"
        set +e
        if [[ ${root_id_flag} -eq 1 ]]; then
          bash -c "${install_cmd}"
          if [[ $? -ne 0 ]]; then
            echo "bash -c \"${install_cmd}\"" >> ${err_file}
          fi
        else
          echo ${passwd} | sudo -S bash -c "${install_cmd}"
          if [[ $? -ne 0 ]]; then
            echo "echo ${passwd} | sudo -S bash -c \"${install_cmd}\"" >> ${err_file}
          fi
        fi
        set -e
      fi
  done < ${apt_list}

  local err_file_bak=${ROOT_DIR}/err_file_bak.txt
  rm -rf ${err_file_bak:?}
  if [[ -f ${err_file} ]]; then
    while read install_cmd; do
      echo -e "sencond: >>> ${install_cmd}"
      set +e
      bash -c "${install_cmd}"
      if [[ $? -ne 0 ]]; then
        echo "${install_cmd}" >> ${err_file_bak}
      fi
      set -e
    done < ${err_file}
  fi

  if [[ -f ${err_file_bak} ]]; then
    echo "============================================================================"
    echo "${err_file} 记录了没有安装成功的软件, 请手动安装"
    echo "============================================================================"
    mv ${err_file_bak} ${err_file}
  fi

  echo -e "apt_get install end"
}

function init_vim_config() {
  echo "start install vim..."
  pushd ~
  git clone https://gitee.com/itopen/vim.git
  cd vim/vim
  bash build.sh
  cd
  rm ~/vim -rf
  popd
}

function show_log() {
  if [[ -f ${err_file} ]]; then
    echo "============================================================================"
    echo "${err_file} 记录了没有安装成功的软件, 请手动安装"
    echo "============================================================================"
  else
    echo "root init success. ^_^"
  fi
}

function root_init() {
  root_menu
  echo -e "确认是否安装 [Y/n]"
  read confirm
  if [[ ! (${confirm} == "Y" || ${confirm} == "y" || ${confirm} == "") ]]; then
    echo "exit install"
    exit 0
  fi

  # 更新镜像源
  update_source_list

  # 切换sh指向bash
  update_sh_to_bash

  # 安装应用
  apt_install

  # git lfs安装
  git_lfs_install

  # repo安装
  repo_install

  # 创建/usr/include/asm
  create_asm_link

  # 展示初始化结果
  show_log
}

function user_init() {
  user_menu
  echo -e "确认是否安装 [Y/n]"
  read confirm
  if [[ ! (${confirm} == "Y" || ${confirm} == "y" || ${confirm} == "") ]]; then
    echo "exit install"
    exit 0
  fi

  # 配置.bashrc中的PS1
  source ~/.bashrc
  start_flag="#############################PersonalSettingStart#############################"
  end_flag="#############################PersonalSettingEnd#############################"
  config_file=$(cd ~ && pwd)/.bashrc
  if [[ $(grep -c ${start_flag} ${config_file}) == "0" && $(grep -c ${end_flag} ${config_file}) == "0" ]]; then
cat >> ~/.bashrc << EOF

${start_flag}
# set PS1
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\n\$ '

export PATH=\$PATH:~/.local/bin
export PATH=\$PATH:~/bin:~/bin/clangd_13.0.0/bin
${end_flag}

clear
EOF
  fi

  # 配置tools
  if [[ ! -d ~/bin ]]; then
    mkdir -p ~/bin
  fi
  cp -rf ${TOOLS_DIR:?}/* ~/bin

  # 配置ssh
  if [[ ! -d ~/.ssh ]]; then
    ssh-keygen -t ed25519 -N "SSH Key" -f ~/.ssh/id_ed25519 -q
  fi

  # 设置ssh连接不用确认
  echo "stricthostkeychecking no" >> ~/.ssh/config

  # 配置git
  echo "请输入git用户名, 不要使用中文名"
  read username
  git config --global user.name ${username}

  echo "请输入git邮箱"
  read email
  git config --global user.email ${email}
  git config --global core.editor vim
  git config --global credential.helper store
  git config --global http.sslverify false

  # 配置vim
  init_vim_config

  # 配置python3的下载源, Ubuntu18.04不支持pip3 config
  set +e
  pip3 config set global.index-url https://pypi.tuna.tsinghua.edu.cn/simple
  if [[ $? -ne 0 ]]; then
    pip3_version=$(echo "$(pip3 --version)" | awk '{print $2}')
    echo "========================================================"
    echo "pip3 版本${pip3_version}太低, 不支持通过config配置python国内源方法"
    echo "========================================================"
  fi
  set -e

  # 配置hb
  set +e
  which hb
  if [[ $? -ne 0 ]]; then
    pushd ${ROOT_DIR}
    if [[ -d ${ROOT_DIR}/hb_build_config ]]; then
      rm -rf ${ROOT_DIR:?}/hb_build_config
    fi
    git clone https://gitee.com/openharmony/build.git hb_build_config
    pip3 install hb_build_config/hb
    # Ubuntu18.04 python3 默认版本太低了, 必须要python3.8以上
    if [[ $? -ne 0 ]]; then
      ubuntu_version=Ubuntu$(echo "$(lsb_release -r)" | awk '{print $2}')
      python3_version=$(echo "$(python3 --version)" | awk '{print $2}')
      echo "======================================================================"
      echo "${ubuntu_version}默认python3版本${python3_version}是太低了, 请将版本升级到python3.8或以上"
      echo "======================================================================"
    fi
    rm -rf hb_build_config
    popd
  fi
  set -e
}

function args_check() {
  main_menu
  read mode

  while read line
  do
    if [[ ${line} == "" || ${line:0:1} == "#" ]]; then
      continue
    fi
    if [[ ${mode} == $(echo ${line} | awk '{print $1}') ]]; then
      break
    fi
  done < ${param_file}

  if [[ $(echo ${line} | awk '{print $1}') == "" ]]; then
    echo "mode is not exist!"
    return 0
  fi

  exec_func=$(echo ${line} | awk '{print $2}')
  eval ${exec_func}
  return 0
}

clear
args_check
