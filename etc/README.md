# 一、Ubuntu版本对应说明

| 名称   | Ubuntu版本 |
| ------ | ---------- |
| bionic | Ubuntu18   |
| focal  | Ubuntu20   |
| jammy  | Ubuntu22   |
| noble  | Ubuntu24   |

